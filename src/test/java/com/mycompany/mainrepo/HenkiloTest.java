/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mainrepo;

import org.junit.Test;
//import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.*; 

/**
 * Testaa Henkilo-luokan
 * 
 * @author juhani, jonna
 */
public class HenkiloTest {
    
    public HenkiloTest() {
    }
    
    /**
    * Testaa palauttaako henkilön nimen oikein
    * @author juhani
    */
    @Test
    public void testPalauttaaNimen() {
        Henkilo matti = new Henkilo("Matti Nykänen", "Kauppalatu 21, 40240 Jyväskylä", "040 123 4567");
        assertEquals("Matti Nykänen", matti.getNimi(), "pitää palauttaa nimen 'Matti Nykänen'");
    }
    
    /**
    * Testaa palauttaako henkilön osoitteen oikein
    * @author juhani
    */
    @Test
    public void testPalauttaaOsoitteen() {
        Henkilo matti = new Henkilo("Matti Nykänen", "Kauppalatu 21, 40240 Jyväskylä", "040 123 4567");
        assertEquals("Kauppalatu 21, 40240 Jyväskylä", matti.getOsoite(), "pitää palauttaa osoitteen 'Kauppalatu 21, 40240 Jyväskylä'");
    }
    
    /**
    * Testaa palauttaako henkilön puhelinnumeron oikein
    * @author juhani
    */
    @Test
    public void testPalauttaaPuhnron() {
        Henkilo matti = new Henkilo("Matti Nykänen", "Kauppalatu 21, 40240 Jyväskylä", "040 123 4567");
        assertEquals("040 123 4567", matti.getPuhnro(), "pitää palauttaa puhelinnumeron '040 123 4567'");
    }
    
    /**
    * Testaa palauttaako olion muuttujat oikein
    * @author jonna
    */
    @Test
    public void testHenkilonOminaisuudet() {
        //assertEqueals in junit jupiter -> expected, actual, message
        Henkilo mauno = new Henkilo("Mauno Maunonen", "Kauppalatu 21, 40240 Jyväskylä", "040 123 4567");
        assertAll("henkilo",
                () -> assertEquals("Mauno Maunonen", mauno.getNimi(),"pitää palauttaa nimen Mauno Maunonen"),
                () -> assertEquals("Kauppalatu 21, 40240 Jyväskylä", mauno.getOsoite(),"pitää palauttaa osoiteen Kauppalatu 21, 40240 Jyväskylä"),
                () -> assertEquals("040 123 4567", mauno.getPuhnro(),"pitää palauttaa puhelinnumero 040 123 4567")
                );  
    }
    
    /**
    * Testaa poistetaanko välilyönnit merkkijonon alusta ja lopusta.
    * @author juhani
    */
    @Test
    public void testNimenAlussaWhiteSpace() {
        Henkilo matti = new Henkilo("    Matti Nykänen  ", "Kauppalatu 21, 40240 Jyväskylä", "040 123 4567");
        assertEquals("Matti Nykänen", matti.getNimi(),"pitää palauttaa nimen Matti Nykänen");
    }
        
}
