
package com.mycompany.mainrepo;

import java.util.HashMap;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Testaa Main luokan
 * @author juhani
 */
public class MainTest {

    public MainTest() {
    }

    /**
     * Testaa onko osoitenkirja luonnin jälkeen tyhjä
     * @author juhani
     */
    @Test
    public void testOsoitekirjaAluksiTyhja() {
        HashMap<String, Henkilo> osoitekirja = new HashMap<>();
        assertTrue("Osoitekirja tulee olla tyhjä luonnin jälkeen.", osoitekirja.isEmpty());
        // fail("The test case is a prototype.");
    }
    
    /**
     * Testaa ettei osoitenkirja luonnin jälkeen ole Null
     * @author juhani
     */
    @Test
    public void testOsoitekirjaAluksiEiOleNull() {
        HashMap<String, Henkilo> osoitekirja = new HashMap<>();
        assertNotNull(osoitekirja);
    }
    
    /**
     * Testaa onko osoitenkirjan koon lisäämisten jälkeen
     * @author juhani
     */
    @Test
    public void testOsoitekirjanKokoLisaamisenJalkeen() {
        HashMap<String, Henkilo> osoitekirja = new HashMap<>();
        // testaamista varten luodaan 3 henkilöä...
        Henkilo matti = new Henkilo("Matti Nykänen", "Kauppalatu 21, 40240 Jyväskyla", "040 123 4567");
        Henkilo jukka = new Henkilo("Jukka Jalava", "Aleksi 10, 15110 Lahti", "040 023 8793");
        Henkilo jukka2 = new Henkilo("Jukka Jalonen", "Korikatu 3 A 7, 06780 Jalava", "0205 3342 553");
        // ... ja lisätään ne osoitekirjaan
        osoitekirja.put(matti.getNimi(), matti);
        osoitekirja.put(jukka.getNimi(), jukka);
        osoitekirja.put(jukka2.getNimi(), jukka2);
        assertTrue("Osoitekirja koko tulee olla 3 lisäämisten jälkeen.", osoitekirja.size() == 3);
    }
}
