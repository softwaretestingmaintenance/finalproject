package com.mycompany.mainrepo;
import org.apache.commons.lang3.StringUtils;

/**
 * Henkilo-olio
 * Sisältää merkkijonomuuttujat nimelle, osoitteelle ja puhelinnnumerolle.
 * @author juhani
 */
public class Henkilo {

    private final String nimi;
    private final String osoite;
    private final String puhnro;

    // Konstruktori Henkilo(String nimi, String osoite, String puhnro)
    // StringUtils.strip() poistaa whitespaces merkkijonon alusta ja lopusta.
    /**
     * Konstruktori Henkilo(String nimi, String osoite, String puhnro) luo uuden
     * Henkilö-olion. StringUtils.strip() poistaa whitespaces merkkijonon alusta
     * ja lopusta.
     * 
     * @param nimi      Henkilön nimi
     * @param osoite    Henkilön osoite
     * @param puhnro    Henkilön puhelinnumero
     */
    public Henkilo(String nimi, String osoite, String puhnro) {
        this.nimi = StringUtils.strip(nimi);
        this.osoite = StringUtils.strip(osoite);
        this.puhnro = StringUtils.strip(puhnro);
    }
    
    /**
     * getter getNimi() palauttaa nimen
     * 
     * @return nimi Palauttaa henkilön nimen
     */
    // getter getNimi() palauttaa nimen
    public String getNimi() {
        return nimi;
    }
    
    /**
     * getter getOsoite() palauttaa osoitteen
     * 
     * @return osoite Palauttaa henkilön osoitteen
     */
    // getter getOsoite() palauttaa osoitteen
    public String getOsoite() {
        return osoite;
    }
    
    /**
     * getter getPuhnro() palauttaa puhelinnumeron
     * 
     * @return puhnron Palauttaa henkilön puhelinnumeron
     */
    // getter getPuhnro() palauttaa puhelinnumeron
    public String getPuhnro() {
        return puhnro;
    }
    
    /*
    * toString Overreide palauttaa merkkijonon:
    * Nimi: <henkilön nimi>
    * Osoite: <henkilöm osoite>
    * Puhnro: <henkilön puhelinnumero>
    */
    /**
     * toString Overreide palauttaa Henkilö-olion tiedot merkkijonona
     * 
     * @return Palauttaa merkkijonon "Nimi: " + this.nimi + "\nOsoite: " + 
     * this.osoite + "\nPuhnro: " + this.puhnro +"\n"
     */
    @Override
    public String toString() {
        return "Nimi: " + this.nimi + "\nOsoite: " + this.osoite + "\nPuhnro: " + this.puhnro +"\n";
    }
}
