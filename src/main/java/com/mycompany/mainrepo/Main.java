package com.mycompany.mainrepo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Ohjelma pitää yllä osoitekirjaa henkilöistä. Henkilöitä voidaan lisätä, 
* etsiä, poistaa, muokata tai tulostaa kaikki henkilöt. Henkilöt on oliota, 
* joihin tallennetaan henkilön nimi, osoite ja puhelinnumero. Henkilö-oliot 
* säilötään HashTableen.
* 
 * @author juhani, jonna
 */
public class Main {

    /**
     * Säiliö Henkilö-olioiden säilömiseen osoitekirjassa. Säiliö on HashMap<key, value>.
     * key kts. luoAvain()
     * value Henkilö-olio
     */
    static HashMap<String, Henkilo> osoitekirja;
    
    /**
     * henkilo on Henkilo-olio, joka sisältää
     * henkilön nimen, osoitteen ja puhelinnumeron
     */
    public Henkilo henkilo;
    
    /**
     * Ohjelma pitää yllä osoitekirjaa henkilöistä. Henkilöitä voidaan lisätä, 
     * etsiä, poistaa, muokata tai tulostaa kaikki henkilöt. Henkilöt on oliota, 
     * joihin tallennetaan henkilön nimi, osoite ja puhelinnumero. Henkilö-oliot 
     * säilötään HashTableen.
     * 
     * @param args Taulukko komentorivin argumenteista sovellukselle
     */
    public static void main(String args[]) {

        Scanner lukija = new Scanner(System.in);

        /*
         * hajautustaulu osoitekirjalle
         * key = String
         * value = Henkilo
         */
        osoitekirja = new HashMap<String, Henkilo>();
        
        // testaamista varten luodaan 3 henkilöä...
        Henkilo matti = new Henkilo("Matti Nykänen", "Kauppalatu 21, 40240 Jyväskyla", "040 123 4567");
        Henkilo jukka = new Henkilo("Jukka Jalava", "Aleksi 10, 15110 Lahti", "040 023 8793");
        Henkilo jukka2 = new Henkilo("Jukka Jalonen", "Korikatu 3 A 7, 06780 Jalava", "0205 3342 553");
        // ... ja lisätään ne osoitekirjaan
        osoitekirja.put(luoAvain(matti.getNimi(), matti.getOsoite(), matti.getPuhnro()), matti);
        osoitekirja.put(luoAvain(jukka.getNimi(), jukka.getOsoite(), jukka.getPuhnro()), jukka);
        osoitekirja.put(luoAvain(jukka2.getNimi(), jukka2.getOsoite(), jukka2.getPuhnro()), jukka2);

        tulostaMenu();
        String valinta = lukija.nextLine();
        String nimi = null;

        while (!valinta.equals("0")) {
            switch (valinta) {
                case "1": // Lisää henkilö
                    System.out.print("Anna nimi: ");
                    nimi = lukija.nextLine();
                    System.out.print("Anna osoite: ");
                    String osoite = lukija.nextLine();
                    System.out.print("Anna puhnro: ");
                    String puhnro = lukija.nextLine();
                    // ..lisätään osoitekirjaan
                    osoitekirja.put(luoAvain(nimi, osoite, puhnro), new Henkilo(nimi, osoite, puhnro));
                    break;

                case "2": // Hae henkilö(t)
                    System.out.println("Haen osoitetiedot");
                    System.out.print("Anna nimi: ");
                    nimi = lukija.nextLine();
                    System.out.println("");
                    // käydään osoitekirja Henkilo kerrallaan
                    // tulostaa kaikki osoitetiedot, joiden nimi sisältää haettavan nimen
                    haeOsoitekirjasta(nimi);
                    break;

                case "3": // Poista henkilö(t)
                    System.out.print("Anna nimi: ");
                    nimi = lukija.nextLine();
                    poistaOsoitekirjasta(nimi);
                    break;

                case "4": // Listaa henkilöt
                    tulostaOsoitekirja();
                    break;
                
                case "5": // Muokkaa henkilöitä
                    System.out.print("Anna nimi: ");
                    nimi = lukija.nextLine();
                   //hae ensin osoitekirjasta henkilöitä nimen perusteella
                    ArrayList<Henkilo> list = haeListaHenkiloista(nimi);
                    if(list.size()<1) break;    //ei löydy
                    if(list.size()>1){  //löytyy useampi samalla nimellä
                        System.out.print("\nAnna tarkempi nimi: ");
                        nimi = lukija.nextLine();
                        list = haeListaHenkiloista(nimi);
                    }
                    //kysy uudet osoite, puhnro tiedot ja päivitä
                    System.out.print("Anna uusi osoite: ");
                    osoite = lukija.nextLine();
                    System.out.print("Anna uusi puhnro: ");
                    puhnro = lukija.nextLine();
                    muokkaaHenkiloa(nimi, osoite, puhnro);
                    System.out.println("\nTiedot päivitetty:\n");
                    haeOsoitekirjasta(nimi);    //hae uudet tiedot
                    break;

                default:
                    System.out.println("\nTuntematon valinta.");
                    break;
            }

            tulostaMenu();
            valinta = lukija.nextLine();
        }

    }
    
    /**
     * Tulostaa Menun.
     */
    // tulostaa Menun
    static void tulostaMenu() {
        System.out.println("\nMenu:\n");
        System.out.println("1   Lisää henkilö");
        System.out.println("2   Hae henkilö");
        System.out.println("3   Poista henkilö");
        System.out.println("4   Listaa henkilöt");
        System.out.println("5   Muokkaa henkilöitä");
        System.out.println("0   Lopeta\n");
        System.out.print("Anna valintasi: ");
    }
    
    /**
     * Luo yksilöllisen avaimen ja lisää osoitekirjaan henkilön annettujen parametrien mukaan.
     * 
     * @param nimi      Henkilön nimi
     * @param osoite    Henkilön osoite
     * @param puhnro    Henkilön puhelinnumero
     */
    // lisää henkilon osoitekirjaan
    public static void lisaaOsoitekirjaan(String nimi, String osoite, String puhnro) {
        // ..lisätään osoitekirjaan
        osoitekirja.put(luoAvain(nimi, osoite, puhnro), new Henkilo(nimi, osoite, puhnro));
    }

    // hakee henkilon tiedot nimellä osoitekirjasta
    /**
     * Hakee merkkijonolla nimi ja tulostaa henkilon löytyneiden tiedot osoitekirjasta
     * 
     * @param nimi  Henkilön nimi
     */
    public static void haeOsoitekirjasta(String nimi) {
        // käydään osoitekirja Henkilo kerrallaan
        // tulostaa kaikki osoitetiedot, joiden nimi sisältää haettavan nimen
        
        boolean loytyi = false;
        
        // tarkistetaan onko osoitekirja tyhjä
        if (!onkoTyhja()) {
            for (String haku : osoitekirja.keySet()) {
                if (osoitekirja.get(haku).getNimi().contains(nimi)) {
                    System.out.println(osoitekirja.get(haku));
                    loytyi = true;
                }
            }
        }
        
        //.. tulostetaan jos ei löytynyt hakusanalla
        if (!loytyi) {
            System.out.println("Ei löytynyt haulla: " + nimi + "!");
        }
    }
    /**
     * Hakee listan henkilöistä nimen perusteella
     * 
     * @param nimi Henkilön nimi
     * @return ArrayList Lista henkilöistä, jotka on haettu nimen perusteella.
     * @see Main#luoAvain(java.lang.String, java.lang.String, java.lang.String) 
     */
    public static ArrayList<Henkilo> haeListaHenkiloista(String nimi){
        boolean loytyi = false;
        
        ArrayList<Henkilo> list = new ArrayList<Henkilo>();
        // tarkistetaan onko osoitekirja tyhjä
        if (!onkoTyhja()) {
            for (String haku : osoitekirja.keySet()) {
                if (osoitekirja.get(haku).getNimi().contains(nimi)) {
                    list.add(osoitekirja.get(haku));    //lisää henkilö listaan
                    loytyi = true;
                }
            }
            //jos listassa on useampi henkilö samalla nimellä
            if(list.size() > 1){
                System.out.println("\nListassa on useampi henkilö.");
                for (Henkilo hlo: list)     //tulosta henkilöt
                    { 
                        System.out.println("\nNimi: " + hlo.getNimi() + "\nOsoite: " + 
                                           hlo.getOsoite() + "\nPuhnro: " + 
                                           hlo.getPuhnro()); 
                    } 
                }
        }
        //.. tulostetaan jos ei löytynyt hakusanalla
        if (!loytyi) {
            System.out.println("Ei löytynyt henkilöä: " + nimi + ".");
        }
        return list;
    }
    /**
     * Muokkaa henkilön osoitetta ja puhelinnumeroa (poistaa vanhan)
     * 
     * @param nimi      Henkilön nimi
     * @param osoite    Henkilön osoite
     * @param puhnro    Henkilön puhelinnumero
     */
    public static void muokkaaHenkiloa(String nimi, String osoite, String puhnro){
        poistaOsoitekirjasta(nimi);
        Henkilo uusiHenkilo = new Henkilo(nimi, osoite, puhnro); 
        osoitekirja.put(luoAvain(nimi,osoite,puhnro), uusiHenkilo);
    }
    
    /**
     * Poistaa osoitekirjasta merkkijonon nimi sisältävät osoitetiedot.
     * 
     * @param nimi Henkilön nimi
     */
    public static void poistaOsoitekirjasta(String nimi) {
        // tarkistetaan onko osoitekirja tyhjä
        if (!onkoTyhja()) {
            Iterator<Henkilo> iterator = osoitekirja.values().iterator();
            
            while (iterator.hasNext()) {
                Henkilo iHenkilo = iterator.next();
                if (iHenkilo.getNimi().contains(nimi)) {
                    iterator.remove();
                }
            }
        }
    }
    
    /**
     * Tulostaa osoitekirjan
     */
    public static void tulostaOsoitekirja() {
        // Tarkistetaan onko osoitekirja tyhjä
        if (!onkoTyhja()) {
                osoitekirja.keySet().forEach((haku) -> {
                System.out.println(osoitekirja.get(haku));
            });
        }
    }
    
    /**
     * Osoitekirjan ollessa tyhjä palauttaa arvon true ja tulostaa merkkijonon 
     * "Osoitekirja on tyhjä!" ja muissa tapauksissa arvon false.
     * 
     * @return boolean Palauttaa totuusarvon onko osoitekirja tyhjä
     */
    public static boolean onkoTyhja(){
        if (osoitekirja.isEmpty()) {
            System.out.println("Osoitekirja on tyhjä!");
            return true;
        }
        return false;
    }
    
    /**
     * Luo yksilöllisen avaimen parametreinä syötettävien merkkijonojen mukaan.
     * Metodi palauttaa merkkijonon, joka on heksadesimaali muodossa.
     * 
     * @param nimi      Henkilön nimi
     * @param osoite    Henkilön osoite
     * @param puhNro    Henkilön puhelinnumero
     * @return String   Palauttaa merkkijonon, joka on muodostettu parametreinä
     * syötettyjen merkkijonojen mukaan. Merkkijono on yksilöllinen heksadesimaali 
     * luku.
     * 
     * <pre>
     * Luku muodostetaan seuraavasti:
     *      int key = 17;
     *      key = 31 * key + nimi.hashCode();
     *      key = 31 * key + osoite.hashCode();
     *      key = 31 * key + puhNro.hashCode();
     * 
     *      String hashKey = Integer.toHexString(key);
     * </pre>
     */
    
    public static String luoAvain(String nimi, String osoite, String puhNro) {
        int key = 17;
        key = 31 * key + nimi.hashCode();
        key = 31 * key + osoite.hashCode();
        key = 31 * key + puhNro.hashCode();
        
        String hashKey = Integer.toHexString(key);
        return hashKey;
    }
}
