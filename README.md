# Osoitekirja

Ohjelma pitää yllä osoitekirjaa henkilöistä. Henkilöitä voidaan lisätä, etsiä, poistaa, muokata tai tulostaa kaikki henkilöt. Henkilöt on oliota, joihin tallennetaan henkilön nimi, osoite ja puhelinnumero. Henkilö-oliot säilötään HashTableen <key, value>.

## Vaatimukset

Tarvittavat ohjelmat oltava asennettuna:

- Java (https://www.oracle.com/technetwork/java/javase/downloads/index.html)
- Netbeans (https://netbeans.org/community/releases/82/install.html)
- Maven (https://maven.apache.org/install.html)

## Testien ajaminen

```
mvn test
```

## Surefire reports & Javadoc

```
mvn site
```

## Teknologiat

- Java 8.0.221
- Maven 3.6.2
- JUnit 4.12 & JUnit Jupiter 5.5.2
- Surefire 3.0.0-M3

### Tekijät

-Kari Hämäläinen

-Jonna Jääskeläinen

-Juhani Pöyry

-Jani Kronlöf
